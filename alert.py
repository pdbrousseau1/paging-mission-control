import argparse
import os
import json
from datetime import datetime, timedelta
from typing import NamedTuple, List, Dict, Any
import pandas as pd


class Config(NamedTuple):
    input_file: str
    columns: list


def get_config() -> Config:
    """
    Parse command-line arguments and return a Config named tuple.

    Returns:
        Config: A named tuple containing the program configuration.
    """
    parser = argparse.ArgumentParser(description="Crater Detection")
    parser.add_argument(
        "--input-file",
        type=str,
        required=True,
        help="Telemetry data input file",
    )

    args = parser.parse_args()

    def check_valid_path(arg_name: str, path: str) -> bool:
        """
        Check if the provided path is a valid file path.

        Args:
            arg_name (str): The name of the argument.
            path (str): The file path to check.

        Returns:
            bool: True if the path is valid, False otherwise.
        """
        if path is not None and not os.path.exists(path):
            raise Exception(f"The {arg_name} arg is not a valid file path: {path}")
        else:
            return True

    check_valid_path("--input-file", args.input_file)

    columns = [
        "timestamp",
        "satellite_id",
        "red_high_limit",
        "yellow_high_limit",
        "yellow_low_limit",
        "red_low_limit",
        "raw_value",
        "component",
    ]

    return Config(input_file=os.path.abspath(args.input_file), columns=columns)


def read_file(file_path: str, columns: list, delimiter: str = "|") -> pd.DataFrame:
    """
    Read a file into a pandas DataFrame.

    Args:
        file_path (str): The path to the input file.
        columns (list): List of column names for the DataFrame.
        delimiter (str, optional): The delimiter used in the file. Defaults to "|".

    Returns:
        pd.DataFrame: The resulting DataFrame.
    """
    telemetry_df = pd.read_csv(file_path, sep=delimiter, header=None)
    telemetry_df.columns = columns
    return telemetry_df


def convert_timestamp(input_dt: str) -> str:
    """
    Convert a timestamp to a specific format.

    Args:
        input_dt (str): The input timestamp.

    Returns:
        str: The formatted timestamp.
    """
    dt_col = datetime.strptime(input_dt, "%Y%m%d %H:%M:%S.%f")
    return dt_col.strftime("%Y-%m-%dT%H:%M:%S.%f")


def window_check(input_df: pd.DataFrame, severity_field: str) -> List[Dict[str, Any]]:
    """
    Check for for rows in DataFrame that fall within 5 minute window.

    Args:
        input_df (pd.DataFrame): The input DataFrame.
        severity_field (str): The severity field to be assigned.

    Returns:
        List[Dict[str, Any]]: List of dictionaries representing alerts.
    """
    alert_list = []
    input_df = input_df.sort_values(by="timestamp").reset_index(drop=True)
    for index, row in input_df.iterrows():
        current_timestamp = row["timestamp"]

        # Check if there are enough rows remaining
        if index + 2 <= len(input_df):
            # Check if the next two rows fall within 5 minutes
            next_rows = input_df.loc[index + 1 : index + 2]
            time_difference_1 = next_rows.iloc[0]["timestamp"] - current_timestamp
            time_difference_2 = next_rows.iloc[1]["timestamp"] - current_timestamp

            if time_difference_1 <= timedelta(
                minutes=5
            ) and time_difference_2 <= timedelta(minutes=5):
                alert_list.append(
                    {
                        "satelliteId": int(row.satellite_id),
                        "severity": severity_field,
                        "component": row.component,
                        "timestamp": current_timestamp.strftime(
                            "%Y-%m-%dT%H:%M:%S.%fZ"
                        )[:-4] + "Z",
                    }
                )
            break

    return alert_list


def monitor_batt(satellite_df: pd.DataFrame) -> List[Dict[str, Any]]:
    """
    Monitor rows in DataFrame specific to battery voltage readings.

    Args:
        satellite_df (pd.DataFrame): The input DataFrame.

    Returns:
        List[Dict[str, Any]]: List of dictionaries representing alerts when
        there are three battery voltage readings that are under the red low
        limit within a five minute interval.
    """
    if len(satellite_df) >= 3:
        satellite_df.reset_index(drop=True)
        low_batt = satellite_df[
            satellite_df["raw_value"] < satellite_df["red_low_limit"]
        ]

        if len(low_batt) >= 3:
            alert_list = window_check(low_batt, "RED LOW")
            return alert_list


def monitor_tstat(satellite_df: pd.DataFrame) -> List[Dict[str, Any]]:
    """
    Monitor rows in DataFrame specific to thermostat readings.

    Args:
        satellite_df (pd.DataFrame): The input DataFrame.

    Returns:
        List[Dict[str, Any]]: List of dictionaries representing alerts where
        there are three thermostat readings that exceed the red high limit
        within a five minute interval.
    """
    if len(satellite_df) >= 3:
        high_tstat = satellite_df[
            satellite_df["raw_value"] > satellite_df["red_high_limit"]
        ]
        if len(high_tstat) >= 3:
            tstat_list = window_check(high_tstat, "RED HIGH")
            return tstat_list


def main(config: Config):
    """
    The main entrypoint of the script

    :param config: The program configuration
    """

    # Read text file and create a pandas dataframe of the telemetry data.
    telemetry_df = read_file(config.input_file, config.columns)
    telemetry_df["timestamp"] = telemetry_df["timestamp"].apply(
        lambda x: convert_timestamp(x)
    )
    telemetry_df["timestamp"] = pd.to_datetime(
        telemetry_df["timestamp"], format="%Y-%m-%dT%H:%M:%S.%f"
    )

    # Create a list of distinct dataframes based on satellite_id column.
    satellite_list = [
        satellite_df for _, satellite_df in telemetry_df.groupby("satellite_id")
    ]

    # Perfrom battery check.
    batt_alert_list = [
        monitor_batt(df[df["component"] == "BATT"])
        for df in satellite_list
        if monitor_batt(df[df["component"] == "BATT"])
    ]

    # Perfrom thermostat check.
    tstat_alert_list = [
        monitor_tstat(df[df["component"] == "TSTAT"])
        for df in satellite_list
        if monitor_tstat(df[df["component"] == "TSTAT"])
    ]

    # Combine alert lists, flatten and represent as json
    final_alert_list = tstat_alert_list + batt_alert_list
    flattened_alerts = [val for sublist in final_alert_list for val in sublist]
    print(json.dumps(flattened_alerts, indent=4))


if __name__ == "__main__":
    main(get_config())
