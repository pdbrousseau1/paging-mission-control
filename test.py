import unittest
from datetime import datetime
from io import StringIO
from alert import (
    read_file,
    window_check,
    monitor_batt,
    monitor_tstat,
)
import pandas as pd

class AlertTest(unittest.TestCase):
    
    test_data = """
                    20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT
                    20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT
                    20180101 23:01:26.011|1001|101|98|25|20|99.8|TSTAT
                    20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT
                    20180101 23:01:49.021|1000|101|98|25|20|87.9|TSTAT
                    20180101 23:02:09.014|1001|101|98|25|20|89.3|TSTAT
                    20180101 23:02:10.021|1001|101|98|25|20|89.4|TSTAT
                    20180101 23:02:11.302|1000|17|15|9|8|7.7|BATT
                    20180101 23:03:03.008|1000|101|98|25|20|102.7|TSTAT
                    20180101 23:03:05.009|1000|101|98|25|20|101.2|TSTAT
                    20180101 23:04:06.017|1001|101|98|25|20|89.9|TSTAT
                    20180101 23:04:11.531|1000|17|15|9|8|7.9|BATT
                    20180101 23:05:05.021|1001|101|98|25|20|89.9|TSTAT
                    20180101 23:05:07.421|1001|17|15|9|8|7.9|BATT
                """

    columns = [
        "timestamp",
        "satellite_id",
        "red_high_limit",
        "yellow_high_limit",
        "yellow_low_limit",
        "red_low_limit",
        "raw_value",
        "component",
    ]

    def test_read_file(self):
        df = read_file(StringIO(self.test_data),self.columns)
        self.assertIsNotNone(df)
        self.assertEqual(list(df.columns), self.columns)
        self.assertEqual(len(df), 14)

    def test_inside_window_check(self):
        inside_window_df = pd.DataFrame({
            'timestamp': [datetime(2022, 1, 1, 23, 1), datetime(2022, 1, 1, 23, 2), datetime(2022, 1, 1, 23, 3)],
            'satellite_id': [1, 1, 1],
            'component': ['BATT', 'BATT', 'BATT'],
        })
        result = window_check(inside_window_df, severity_field="TEST")
        self.assertEqual(len(result), 1)
    
    def test_outside_window_check(self):
        outside_window_df = pd.DataFrame({
            'timestamp': [datetime(2022, 1, 1, 23, 1), datetime(2022, 1, 1, 23, 10), datetime(2022, 1, 1, 23, 10)],
            'satellite_id': [1, 1, 1],
            'component': ['BATT', 'BATT', 'BATT'],
        })
        result = window_check(outside_window_df, severity_field="TEST")
        self.assertEqual(len(result), 0)

    def test_monitor_batt_below(self):
        df = pd.DataFrame({
            'timestamp': [datetime(2022, 1, 1, 23, 1), datetime(2022, 1, 1, 23, 2), datetime(2022, 1, 1, 23, 3)],
            'satellite_id': [1, 1, 1],
            'component': ['BATT', 'BATT', 'BATT'],
            'raw_value': [70, 70, 70],
            'red_low_limit': [80, 80, 80],
        })
        result = monitor_batt(df)
        self.assertEqual(len(result), 1)
    
    def test_monitor_batt_no_alert(self):
        df = pd.DataFrame({
            'timestamp': [datetime(2022, 1, 1, 23, 1), datetime(2022, 1, 1, 23, 2), datetime(2022, 1, 1, 23, 3)],
            'satellite_id': [1, 1, 1],
            'component': ['BATT', 'BATT', 'BATT'],
            'raw_value': [81, 70, 70],
            'red_low_limit': [80, 80, 80],
        })
        result = monitor_batt(df)
        self.assertIsNone(result)
    
    def test_monitor_tstat_above(self):
        df = pd.DataFrame({
            'timestamp': [datetime(2022, 1, 1, 23, 1), datetime(2022, 1, 1, 23, 2), datetime(2022, 1, 1, 23, 3)],
            'satellite_id': [1, 1, 1],
            'component': ['TSTAT', 'TSTAT', 'TSTAT'],
            'raw_value': [121, 130, 140],
            'red_high_limit': [120, 120, 120],
        })
        result = monitor_tstat(df)
        self.assertEqual(len(result), 1)

    def test_monitor_tstat_no_alert(self):
        df = pd.DataFrame({
            'timestamp': [datetime(2022, 1, 1, 23, 1), datetime(2022, 1, 1, 23, 2), datetime(2022, 1, 1, 23, 3)],
            'satellite_id': [1, 1, 1],
            'component': ['TSTAT', 'TSTAT', 'TSTAT'],
            'raw_value': [120, 130, 140],
            'red_high_limit': [120, 120, 120],
        })
        result = monitor_tstat(df)
        self.assertIsNone(result)

if __name__ == '__main__':
    unittest.main()